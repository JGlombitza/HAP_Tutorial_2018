import numpy as np
from keras.layers import BatchNormalization, Conv2D, Dense, GlobalMaxPooling2D, Reshape, UpSampling2D
from keras.models import Sequential
from keras.optimizers import Adam
from keras.layers.advanced_activations import LeakyReLU
from functools import partial
from keras.utils import plot_model
import keras.backend.tensorflow_backend as KTF
import utils


KTF.set_session(utils.get_session())  # Allows 2 jobs per GPU, Please do not change this during the tutorial
log_dir = "."

# basic trainings parameter
EPOCHS = 10
GRADIENT_PENALTY_WEIGHT = 10
BATCH_SIZE = 256
NCR = 5
latent_size = 512

# load trainings data
shower_maps, Energy = utils.ReadInData()
N = shower_maps.shape[0]
# plot real signal patterns
utils.plot_multiple_signalmaps(shower_maps[:, :, :, 0], log_dir=log_dir, title='Footprints', epoch='Real')


# build generator
# Feel free to modify the generator model
def build_generator(latent_size):
    generator = Sequential(name='generator')
    generator.add(Dense(latent_size, activation='relu', input_shape=(latent_size,)))
    generator.add(Reshape((1, 1, latent_size)))
    generator.add(UpSampling2D(size=(3, 3)))
    generator.add(BatchNormalization())
    generator.add(UpSampling2D(size=(3, 3)))
    generator.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu'))
    generator.add(BatchNormalization())
    generator.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu'))
    generator.add(BatchNormalization())
    generator.add(Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu'))
    generator.add(BatchNormalization())
    generator.add(Conv2D(1, (3, 3), padding='same', kernel_initializer='he_normal', activation='relu'))
    return generator


# build critic
# Feel free to modify the critic model
def build_critic():
    critic = Sequential(name='critic')
    critic.add(Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal', input_shape=(9, 9, 1)))
    critic.add(LeakyReLU())
    critic.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal'))
    critic.add(LeakyReLU())
    critic.add(GlobalMaxPooling2D())
    critic.add(Dense(100))
    critic.add(LeakyReLU())
    critic.add(Dense(1))
    return critic


generator = build_generator(latent_size)
print(generator.summary())
critic = build_critic()
print(critic.summary())

# make trainings model for generator
utils.make_trainable(critic, False)  # freeze the critic during the generator training
utils.make_trainable(generator, True)  # unfreeze the generator during the generator training

generator_training = utils.build_generator_graph(generator, critic, latent_size)
generator_training.compile(optimizer=Adam(0.0001, beta_1=0.5, beta_2=0.9, decay=0.0), loss=[utils.wasserstein_loss])
plot_model(generator_training, to_file=log_dir + '/generator_training.png', show_shapes=True)

# make trainings model for critic
utils.make_trainable(critic, True)  # unfreeze the critic during the critic training
utils.make_trainable(generator, False)  # freeze the generator during the critic training

critic_training, averaged_batch = utils.build_critic_graph(generator, critic, latent_size, batch_size=BATCH_SIZE)
gradient_penalty = partial(utils.gradient_penalty_loss, averaged_batch=averaged_batch, penalty_weight=GRADIENT_PENALTY_WEIGHT)  # construct the gradient penalty
gradient_penalty.__name__ = 'gradient_penalty'
critic_training.compile(optimizer=Adam(0.0001, beta_1=0.5, beta_2=0.9, decay=0.0), loss=[utils.wasserstein_loss, utils.wasserstein_loss, gradient_penalty])
plot_model(critic_training, to_file=log_dir + '/critic_training.png', show_shapes=True)

# For Wassersteinloss
positive_y = np.ones(BATCH_SIZE)
negative_y = -positive_y
dummy = np.zeros(BATCH_SIZE)  # keras throws an error when calculating a loss without having a label -> needed for using the gradient penalty loss

generator_loss = []
critic_loss = []

# trainings loop
iterations_per_epoch = N//((NCR+1)*BATCH_SIZE)
for epoch in range(EPOCHS):
    print("epoch: ", epoch)
    # plot berfore each epoch generated signal patterns
    generated_map = generator.predict_on_batch(np.random.randn(BATCH_SIZE, latent_size))
    utils.plot_multiple_signalmaps(generated_map[:, :, :, 0], log_dir=log_dir, title='Generated Footprints Epoch: ', epoch=str(epoch))
    for iteration in range(iterations_per_epoch):

        for j in range(NCR):

            noise_batch = np.random.randn(BATCH_SIZE, latent_size)  # generate noise batch for generator
            shower_batch = shower_maps[BATCH_SIZE*(j+iteration):BATCH_SIZE*(j++iteration+1)]  # take batch of shower maps
            critic_loss.append(critic_training.train_on_batch([noise_batch, shower_batch], [negative_y, positive_y, dummy]))  # train the critic
            print("critic loss:", critic_loss[-1])

        noise_batch = np.random.randn(BATCH_SIZE, latent_size)  # generate noise batch for generator
        generator_loss.append(generator_training.train_on_batch([noise_batch], [positive_y]))  # train the generator
        print("generator loss:", generator_loss[-1])

# plot critic and generator loss
utils.plot_loss(critic_loss, name="critic", log_dir=log_dir)
utils.plot_loss(generator_loss, name="generator", log_dir=log_dir)

# plot some generated signal patterns
generated_map = generator.predict(np.random.randn(BATCH_SIZE, latent_size))
utils.plot_multiple_signalmaps(generated_map[:, :, :, 0], log_dir=log_dir, title='Generated Footprints', epoch='Final')
